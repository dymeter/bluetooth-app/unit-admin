import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddCommonComponent } from './add-common.component';

const routes: Routes = [
  {
    path: '',
    component: AddCommonComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddCommonRoutingModule { }

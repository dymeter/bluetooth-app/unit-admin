import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ManageService, RegisterService } from 'src/app/shared/services';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-common',
  templateUrl: './add-common.component.html',
  styleUrls: ['./add-common.component.scss']
})
export class AddCommonComponent implements OnInit {

  form: FormGroup;
  devices: any;
  closeResult: string;

  constructor(
    private manageService: ManageService,
    private registerService: RegisterService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private router: Router,

  ) {

  }

  ngOnInit() {
    this.manageService.deviceList().subscribe((res: any) => {
      this.devices = res;
    });

    this.form =  this.formBuilder.group({
      device_id: ['', [Validators.required]],
      mass: ['', [Validators.required]],
      water: ['', [Validators.required]],
      volume: ['', [Validators.required]],
      air_volume: ['', [Validators.required]],
      pz: ['', [Validators.required]],

    });
  }


  onSubmit() {

    this.registerService.addCommon(this.form.value)
    .subscribe(
      response => {
        console.log('Success!', response);
        this.toastr.success('일시를 추가했습니다.', '일시추가', {
          timeOut: 3000
        });
        setTimeout(()=>{
          this.router.navigate(['/add']);
        }, 1000);
      },
      error => {
        console.error('Error!', error);
        this.toastr.error('서버에 에러가 발생되었습니다.', '에러 발생', {
          timeOut: 3000
        });
      }
    );
    
    this.modalService.dismissAll();
  }
 

  open(content) {
    this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return  `with: ${reason}`;
    }
  }

}

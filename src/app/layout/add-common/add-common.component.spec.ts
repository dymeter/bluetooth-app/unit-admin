import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCommonComponent } from './add-common.component';

describe('AddCommonComponent', () => {
  let component: AddCommonComponent;
  let fixture: ComponentFixture<AddCommonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCommonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

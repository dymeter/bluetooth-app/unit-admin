import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NumberPickerModule } from 'ng-number-picker';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddCommonRoutingModule } from './add-common-routing.module';
import { MatCheckboxModule } from '@angular/material';
import { PageHeaderModule } from 'src/app/shared';
import { AddCommonComponent } from './add-common.component';

@NgModule({
  declarations: [AddCommonComponent],
  imports: [
    CommonModule,
    AddCommonRoutingModule,
    PageHeaderModule,
    ReactiveFormsModule,
    NgbModule,
    NumberPickerModule,
    MatCheckboxModule
  ]
})
export class AddCommonModule { }

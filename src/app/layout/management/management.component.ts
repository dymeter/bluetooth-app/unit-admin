import { ManageService } from './../../shared/services/manage.service';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss'],
  animations: [routerTransition()]
})
export class ManagementComponent implements OnInit {

  devices: any;
  searchText: any;

  constructor(
    private manageService: ManageService,
    public router: Router,

  ) { }

  ngOnInit() {
    this.manageService.deviceList().subscribe((res: any) => {
      this.devices = res;
    });
  }

  routerDetail(href) {
    this.router.navigateByUrl(`/management/details/` + href);
  }

}

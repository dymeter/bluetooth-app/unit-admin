import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },

            { path: 'users', loadChildren: './users/users.module#UsersModule' },
            {
              path: 'management',
              children: [
                {
                  path: '',
                  loadChildren: './management/management.module#ManagementModule'
                },
                {
                  path: 'details/:companyId',
                  loadChildren: './details/details.module#DetailsModule'
                },
              ]
            },
            {
                path: 'add',
                children: [
                  {
                    path: '',
                    loadChildren: './add/add.module#AddModule'
                  },
                  {
                    path: 'add-device',
                    loadChildren: './add-device/add-device.module#AddDeviceModule'
                  },
                  {
                    path: 'add-bluetooth',
                    loadChildren: './add-bluetooth/add-bluetooth.module#AddBluetoothModule'
                  },
                  {
                    path: 'add-common',
                    loadChildren: './add-common/add-common.module#AddCommonModule'
                  },

                ]
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddBluetoothRoutingModule } from './add-bluetooth-routing.module';
import { AddBluetoothComponent } from './add-bluetooth.component';
import { MatCardModule, MatCheckboxModule } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NumberPickerModule } from 'ng-number-picker';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PageHeaderModule } from 'src/app/shared';

@NgModule({
  declarations: [AddBluetoothComponent],
  imports: [
    CommonModule,
    AddBluetoothRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NumberPickerModule,
    NgbModule,
    MatCheckboxModule,
    MatCardModule,
  ]
})
export class AddBluetoothModule { }

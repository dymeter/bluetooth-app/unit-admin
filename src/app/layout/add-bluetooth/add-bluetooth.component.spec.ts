import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBluetoothComponent } from './add-bluetooth.component';

describe('AddBluetoothComponent', () => {
  let component: AddBluetoothComponent;
  let fixture: ComponentFixture<AddBluetoothComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBluetoothComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBluetoothComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

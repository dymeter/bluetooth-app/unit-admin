import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBluetoothComponent } from './add-bluetooth.component';

const routes: Routes = [
  {
    path: '',
    component: AddBluetoothComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddBluetoothRoutingModule { }

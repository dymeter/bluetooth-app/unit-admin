import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddDeviceRoutingModule } from './add-device-routing.module';
import { AddDeviceComponent } from './add-device.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PageHeaderModule } from 'src/app/shared';

@NgModule({
  declarations: [AddDeviceComponent],
  imports: [
    CommonModule,
    AddDeviceRoutingModule,
    PageHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class AddDeviceModule { }

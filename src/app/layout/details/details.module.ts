import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PageHeaderModule } from './../../shared/modules/page-header/page-header.module';
import { DetailsComponent } from './details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { DetailsRoutingModule } from './details-routing.module';
import { NgxGaugeModule } from 'ngx-gauge';
import { SharedPipesModule } from 'src/app/shared';
import { InfoComponent } from './components/info/info.component';
import { TimeSeriesComponent } from './components/time-series/time-series.component';
import { SavedUnitComponent } from './components/saved-unit/saved-unit.component';

@NgModule({
  declarations: [
    DetailsComponent,  
    InfoComponent,
    TimeSeriesComponent,
    SavedUnitComponent
  ],
  imports: [
    CommonModule,
    DetailsRoutingModule,
    PageHeaderModule,
    NgxDatatableModule,
    NgbModule,
    NgxGaugeModule,
    SharedPipesModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    InfiniteScrollModule
  ]
})
export class DetailsModule { }

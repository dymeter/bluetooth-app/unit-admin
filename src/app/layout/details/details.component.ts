import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManageService } from './../../shared/services/manage.service';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ActivatedRoute } from '@angular/router';
import { _ } from 'underscore';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  animations: [routerTransition()],
})
export class DetailsComponent implements OnInit {

  deviceId: string;

  constructor(
    private route: ActivatedRoute,
    ) {

    }

    ngOnInit() {
      this.deviceId = this.route.snapshot.paramMap.get('companyId');

    }
  
}

import { Component, OnInit, Input } from '@angular/core';
import { ManageService } from 'src/app/shared/services';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-saved-unit',
  templateUrl: './saved-unit.component.html',
  styleUrls: ['./saved-unit.component.scss']
})
export class SavedUnitComponent implements OnInit {

  @Input() deviceId: string;
  results : any;
  closeResult: string;
  datas: any;
  page = 0;
  loading: boolean;

  constructor(
    private manageService: ManageService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.manageService.getSavedUnit(this.deviceId, this.page).subscribe((res: any) => {
      this.results = res;
      if(this.results.length == 0) {
        this.loading = true;
      }
    });
  }

  open(content, id) {
    this.manageService.getResult(id).subscribe((res:any) => {
      this.datas = res;
      console.log(res);
      
      this.modalService.open(content).result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return `with: ${reason}`;
    }
  }

  onScroll() {
    console.log('scrolled!!');
    this.page++;
    this.manageService.getSavedUnit(this.deviceId, this.page).subscribe((res: any) => {
      res.forEach(element => {
        this.results.push(element); 
      });
    });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedUnitComponent } from './saved-unit.component';

describe('SavedUnitComponent', () => {
  let component: SavedUnitComponent;
  let fixture: ComponentFixture<SavedUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

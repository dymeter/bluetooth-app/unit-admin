import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { ManageService } from 'src/app/shared/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-time-series',
  templateUrl: './time-series.component.html',
  styleUrls: ['./time-series.component.scss']
})
export class TimeSeriesComponent implements OnInit {

  @ViewChild('content') content: ElementRef;
  //@ViewChild('gmap') mapElement: any;

  @Input() deviceId: string;
  form: FormGroup;
  date = {
    today: '',
    last: ''
  };
  rows = [];
  public loading = false;
  datas: any;
  
  constructor(
    private formBuilder: FormBuilder,
    private manageService: ManageService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal

  ) {

  }

  ionViewDidEnter(){

  }

  ngOnInit() {
    this.date.today = moment().format("YYYY-MM-DD");
    this.date.last = moment().subtract(30, 'days').format("YYYY-MM-DD");

    this.form = this.formBuilder.group({
      device_id: [this.deviceId, [Validators.required]],
      start: new FormControl(this.date.last, Validators.required),
      end: new FormControl(this.date.today, Validators.required),
    });

  }

  onSubmit() {
    this.loading = true;

    this.spinner.show();

    this.manageService.getTimeseries(this.form.value).subscribe((res: any) => {
      if(res.length == 0) {
        alert("해당 데이터가 없습니다.");
        this.spinner.hide();
        this.loading = false;
      }
      this.rows = res;
      this.loading = false;
      this.spinner.hide();
      
    });
  }

  onActivate(event) {
    if(event.type == 'click') {
      let row = event.row;
      this.manageService.getResult(row.id).subscribe((res:any) => {
        this.datas = res;
        this.modalService.open(this.content);
      });

    }
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ManageService } from 'src/app/shared/services';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  @Input() deviceId: string;

  public isCollapsed5: boolean;

  editBluetoothForm: FormGroup;
  editNameForm: FormGroup;
  editCommonForm: FormGroup;

  bluetooths: any;
  commons: any;
  closeResult: string;

  constructor(
    private manageService: ManageService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
  ) { 
    this.isCollapsed5 = true;
  }

  ngOnInit() {

    this.manageService.getBluetooth(this.deviceId).subscribe((res: any) => {
      this.bluetooths = res;
      this.editBluetoothForm = this.formBuilder.group({
        scale_uuid: new FormControl(res[0].scale_uuid, [Validators.required, Validators.pattern('[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}')]),
        airmeter_uuid: new FormControl(res[0].airmeter_uuid, [Validators.required, Validators.pattern('[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}')]),
        printer_uuid: new FormControl(res[0].printer_uuid, [Validators.required, Validators.pattern('[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}')])
      });
      this.editNameForm = this.formBuilder.group({
        scale_name: new FormControl(res[0].scale_name, [Validators.required]),
        airmeter_name: new FormControl(res[0].airmeter_name, [Validators.required]),
      });
    });

    this.manageService.getCommon(this.deviceId).subscribe((res: any) => {
      this.commons = res;
      this.editCommonForm = this.formBuilder.group({
        mass: new FormControl(res[0].mass, [Validators.required, Validators.pattern("^[0-9]*$")]),
        water: new FormControl(res[0].water, [Validators.required, Validators.pattern("^[0-9]*$")]),
        volume: new FormControl(res[0].volume, [Validators.required, Validators.pattern("^[0-9]*$")]),
        air_volume: new FormControl(res[0].air_volume, [Validators.required, Validators.pattern("^[0-9]*$")]),
        pz: new FormControl(res[0].pz, [Validators.required, Validators.pattern("^[0-9]*$")]),
      });
    });
  }

  onBluetoothEdit(id) {
    this.manageService.editBluetooth(id, this.editBluetoothForm.value)
    .subscribe((res: any) => {
      this.manageService.getBluetooth(this.deviceId).subscribe((res: any) => {
        this.bluetooths = res;
      });
      this.toastr.success(res.success, '수정완료', {
        timeOut: 2500
      });
    });
    this.modalService.dismissAll();
  }

  onNameEdit(id) {
    this.manageService.editName(id, this.editNameForm.value)
    .subscribe((res: any) => {
      this.manageService.getBluetooth(this.deviceId).subscribe((res: any) => {
        this.bluetooths = res;
      });
      this.toastr.success(res.success, '수정완료', {
        timeOut: 2500
      });
    });
    this.modalService.dismissAll();
  }


  onCommonEdit(id) {  
    this.manageService.editCommon(id, this.editCommonForm.value)
    .subscribe((res: any) => {
      this.toastr.success(res.success, '수정완료', {
        timeOut: 2500
      });
      this.manageService.getCommon(this.deviceId).subscribe((res: any) => {
        this.commons = res;
      });
    });
    this.modalService.dismissAll();
  }
  
  onDeleteDevice() {
    this.manageService.deleteDevice(this.deviceId)
    .subscribe(
      response => {
        console.log('Success!', response);
        this.toastr.success('회사가 삭제되었습니다.', '회사 삭제', {
          timeOut: 3000
        });
      },
      error => {
        console.error('Error!', error);
      }
    );
    this.modalService.dismissAll();
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return  `with: ${reason}`;
    }
  }

}

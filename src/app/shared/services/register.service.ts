import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  url = environment.url;
  public headers: any;

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router

  ) {
    this.headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + this.auth.getToken());
   }

  addDevice(crendentials) {
    return this.http.post(`${this.url}/add-device`, crendentials, {headers: this.headers}).pipe(
      tap(res => {
        this.toastr.success('디바이스를 추가했습니다.', '디바이스 추가', {
          timeOut: 3000
        });
        setTimeout(()=>{
          this.router.navigate(['/add']);
        }, 1000);
      }),
      catchError(e => {
        this.toastr.error(e.error.error, '에러 발생', {
          timeOut: 3000
        });
        throw new Error(e);
      })
    );
  }

  /**
   * @memberof ManageService
   * 블루투스추가 POST요청
   */
  addBluetooth(credentials) {
    return this.http.post(`${this.url}/add-bluetooth`, credentials, {headers: this.headers}).pipe(
      tap(_ => {
        return true;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  addCommon(credentials) {
    return this.http.post(`${this.url}/add-common`, credentials, {headers: this.headers}).pipe(
      tap(_ => {
        return true;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }


}



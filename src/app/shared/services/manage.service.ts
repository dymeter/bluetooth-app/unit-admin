import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment.prod';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ManageService {
  url = environment.url;
  public headers: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private auth: AuthService,
    private toastr: ToastrService
  ) {
    this.headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + this.auth.getToken());
  }


  dashboard() {

    return this.http.get(`${this.url}/dashboard`, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }


  manageUsers() {
    return this.http.get(`${this.url}/users`, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  /**
   *  회사정보
   * @returns
   * @memberof ManageService
   */
  deviceList() {
    /*
    const headers = new HttpHeaders()
    .set('Authorization', this.auth.getToken());
    */
    return this.http.get(`${this.url}/device-list`, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getBluetooth(device_id) {
    return this.http.get(`${this.url}/bluetooth/` + device_id, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getCommon(device_id) {
    // const params = new HttpParams().set('company', company);
    // return this.http.get(`${this.url}/getchannel/`, {params: params}).pipe(
    return this.http.get(`${this.url}/common/` + device_id, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getTimeseries(credentials) {
    const params = new HttpParams()
      .set('start', credentials.start)
      .set('end', credentials.end)
      .set('deviceid', credentials.device_id);

    return this.http.get(`${this.url}/timeseries`, { headers: this.headers, params: params }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getSavedUnit(device_id, page) {
    const params = new HttpParams()
    .set('page', page);
    return this.http.get(`${this.url}/saved-unit/` + device_id, { headers: this.headers, params: params }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getResult(deviceId) {
    return this.http.get(`${this.url}/unit-result/` + deviceId, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  verifyState(bool, user_id) {
    return this.http.put(`${this.url}/users/verify/` + user_id, {'bool': bool}, {headers: this.headers}).pipe(
      tap(_ => console.log(`updated id=${user_id}`)),
      catchError(e => {
        this.showAlert();
        throw new Error(e);
      })
    );
  }

  deleteUser(user_id) {
    return this.http.delete(`${this.url}/users/` + user_id, { headers: this.headers }).pipe(
      tap(_ => console.log(`updated id=${user_id}`)),
      catchError(e => {
        this.showAlert();
        throw new Error(e);
      })
    );
  }

  editBluetooth(id, crendentials) {
    return this.http.put(`${this.url}/bluetooth/${id}`, crendentials, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        this.showAlert();
        throw new Error(e);
      })
    );
  }

  editName(id, crendentials) {
    return this.http.put(`${this.url}/bname/${id}`, crendentials, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        this.showAlert();
        throw new Error(e);
      })
    );
  }

  editCommon(id, crendentials) {
    return this.http.put(`${this.url}/common/` + id, crendentials, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        this.showAlert();
        throw new Error(e);
      })
    );
  }

  deleteDevice(device_id) {
    return this.http.delete(`${this.url}/device/` + device_id, { headers: this.headers }).pipe(
      tap(res => {
        this.router.navigate(['/management']);
      }),
      catchError(e => {
        this.showAlert();
        throw new Error(e);
      })
    );
  }

   /**
   * 채널삭제
   */
  deleteChannel(channel_id) {
    return this.http.delete(`${this.url}/channel/` + channel_id, { headers: this.headers }).pipe(
      tap(res => {
        return res;
      }),
      catchError(e => {
        this.showAlert();
        throw new Error(e);
      })
    );
  }


  successAlert() {
    this.toastr.success('변경되었습니다.', '변경', {
      timeOut: 3000
    });
  }

  showAlert() {
    this.toastr.error('에러가 발생했습니다.', '에러 발생', {
      timeOut: 3000
    });
  }


}

export const environment = {
  production: true,
  url: 'http://app.dymeter.com:4000/admin',
  
  msg_ok: '정상측정중입니다.',
  msg_network_error: '통신불량입니다.',
  msg_correction: '교정중입니다.',
  msg_replace: '교체중입니다.',
  msg_trouble: '고장입니다.',
  msg_clean: '세척중입니다.',
};
